﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class RadianceBuffer : MonoBehaviour
{
    public enum LobeConfig
    {
        FOUR = 0,
        EIGHT,
        TWELVE,
        SIXTEEN
    };

    public LobeConfig lobe_config;
    public int num_lobes;
    public RenderTexture[] rbuff;
    public RenderTexture energy_buff;
    public Vector4[] lobe_dirs;
    
    public float monte_carlo;
    public float integral;
    public Shader first_pass_shader;
    
    public RenderTexture normal_buf;
    public RenderTexture tangent_buf;
    public RenderTexture depth_buf;

    public Material copy_depth_mat;

    public Texture2D clear_tex;

    public Camera sub_cam;
    public RadianceCam rad_cam;

    public RADCAMDBG rad_cam_state;
    private RADCAMDBG cache_rad_cam_state;

    private RenderTargetSetup target;


    public float sharpness;
    private float cache_sharpness;

    // Use this for initialization
    private void OnEnable()
    {
        Init();
    }

    private void Start()
    {
        Init();
    }

    void Init ()
    {
        int[] lobe_array = { 4, 8, 12, 16 };
        num_lobes = lobe_array[(int)lobe_config];

        int num_tex = (int)num_lobes / 4;
        rbuff = new RenderTexture[num_tex];

        Camera cam = this.GetComponent<Camera>();
        cam.clearFlags = CameraClearFlags.SolidColor;
        cam.backgroundColor = Color.black;
        cam.depthTextureMode = DepthTextureMode.Depth;

        int width = cam.pixelWidth;
        int height = cam.pixelHeight;

        for(int i = 0; i < num_tex; ++i)
        {
            rbuff[i] = new RenderTexture(width, height, 0, RenderTextureFormat.ARGBFloat);
            rbuff[i].filterMode = FilterMode.Point;

            Shader.SetGlobalTexture("_RadBuff" + i, rbuff[i]);
        }

        energy_buff = new RenderTexture(width, height, 0, RenderTextureFormat.ARGBFloat);
        energy_buff.filterMode = FilterMode.Point;
        Shader.SetGlobalTexture("_EnergyBuff", energy_buff);



        string[] keywords = { "SPHERE_GAUSS_4", "SPHERE_GAUSS_8", "SPHERE_GAUSS_12", "SPHERE_GAUSS_16" };
        for(int i = 0; i < keywords.Length; ++i)
        {
            if( i == (int)lobe_config )
            {
                Shader.EnableKeyword(keywords[i]);
            }
            else
            {
                Shader.DisableKeyword(keywords[i]);
            }
        }


        
        int N = 2 * num_lobes;

        float inc = Mathf.PI * (3.0f - Mathf.Sqrt(5.0f));
        float off = 2.0f / N;

        lobe_dirs = new Vector4[16];
        int lobe_idx = 0;

        for (int i = 0; i < N; ++i)
        {
            float z = i * off - 1.0f + (off / 2.0f);
            float r = Mathf.Sqrt(1.0f - z * z);
            float phi = i * inc;
            Vector3 axis = new Vector3( r * Mathf.Cos(phi), r * Mathf.Sin(phi), z).normalized;

            if( Vector3.Dot( axis, Vector3.forward ) > 0.0f )
            {
                lobe_dirs[lobe_idx] = new Vector4(axis.x, axis.y, axis.z, 0);
                ++lobe_idx;
            }
        }
        
        Shader.SetGlobalVectorArray("SG_Axii", lobe_dirs);



        Vector3 a = new Vector3(lobe_dirs[0].x, lobe_dirs[0].y, lobe_dirs[0].z);
        Vector3 b = new Vector3(lobe_dirs[1].x, lobe_dirs[1].y, lobe_dirs[1].z);
        Vector3 h = (a + b).normalized;
        sharpness = (Mathf.Log(0.65f) * (float)num_lobes) / (Vector3.Dot(h, a) - 1.0f);
       // sharpness = SharpnessFromThreshold(1.0f, 0.25f, Vector3.Dot(a, b));

        Shader.SetGlobalFloat("SG_Sharpness", sharpness);
        
        
        GameObject oldCam = GameObject.Find("RB_SecondPass");
        Cleanup(oldCam);
        GameObject camObj = new GameObject("RB_SecondPass");
        sub_cam = camObj.AddComponent<Camera>();
        camObj.transform.position = this.transform.position;
        camObj.transform.rotation = this.transform.rotation;
        sub_cam.CopyFrom(cam);
        sub_cam.depth = cam.depth + 1;
        sub_cam.transform.parent = this.transform;
        sub_cam.targetTexture = null;
        sub_cam.ResetReplacementShader();
        sub_cam.clearFlags = CameraClearFlags.Skybox;

        rad_cam = camObj.AddComponent<RadianceCam>();
        rad_cam.rad_buffer = this;
        rad_cam_state = RADCAMDBG.RB0;


        normal_buf = new RenderTexture(width, height, 0, RenderTextureFormat.ARGB32);
        tangent_buf = new RenderTexture(width, height, 0, RenderTextureFormat.ARGB32);
        depth_buf = new RenderTexture(width, height, 24, RenderTextureFormat.RFloat);
        cam.SetTargetBuffers( 
             new RenderBuffer[] { normal_buf.colorBuffer, tangent_buf.colorBuffer, depth_buf.colorBuffer }, 
             depth_buf.depthBuffer);
        cam.SetReplacementShader(first_pass_shader, "RenderType");



        Shader.SetGlobalTexture("_NormalBuffer", normal_buf);
        Shader.SetGlobalTexture("_TangentBuffer", tangent_buf);
        Shader.SetGlobalTexture("_DBuffer", depth_buf);


        clear_tex = new Texture2D(1, 1, TextureFormat.ARGB32, false);
        clear_tex.SetPixels(new Color[] { new Color(0, 0, 0, 0) });
        clear_tex.Apply();


        float monteCarloFactor = (2.0f * Mathf.PI) / (float)num_lobes;
        Shader.SetGlobalFloat("SG_MonteCarloFactor", monteCarloFactor);
        monte_carlo = monteCarloFactor;


        float expTerm = 1.0f - Mathf.Exp(-2.0f * sharpness);
        float integralTerm = 2.0f * Mathf.PI * (1.0f / sharpness) * expTerm;
        Shader.SetGlobalFloat("SG_IntegralTerm", integralTerm);
        integral = integralTerm;


        RenderBuffer[] col_buffers = new RenderBuffer[rbuff.Length+1];

        col_buffers[0] = energy_buff.colorBuffer;

        for (int i = 0; i < rbuff.Length; ++i)
        {
            col_buffers[i+1] = rbuff[i].colorBuffer;
        }

        target = new RenderTargetSetup(
                col_buffers, depth_buf.depthBuffer
            );
    }

    float SharpnessFromThreshold(float amplitude, float epsilon, float cos_theta)
    {
        return (Mathf.Log(epsilon) - Mathf.Log(amplitude)) / (cos_theta - 1.0f);
    }

    private void Cleanup(GameObject obj)
    {
        if (obj != null)
        {
            if (Application.isEditor)
            {
                DestroyImmediate(obj);
            }
            else
            {
                Destroy(obj);
            }
        }
    }

    private void Destruct()
    {
        if(sub_cam != null) { Cleanup(sub_cam.gameObject);  }
    }

    private void OnDisable()
    {
        Destruct();
    }

    private void OnDestroy()
    {
        Destruct();
    }

    public RenderTargetSetup RadianceTarget
    {
        get
        {
            return target;
        }
    }

    private void OnPostRender()
    {
        var cam = this.GetComponent<Camera>();

        for(int i = 0; i < rbuff.Length; ++i)
        {
            Graphics.Blit(clear_tex, rbuff[i]);
        }

        LightManager.Get().RenderLights(cam, target);
    }

    public void Update()
    {
        if( cache_rad_cam_state != rad_cam_state )
        {
            rad_cam.SetDisplay(rad_cam_state);
            cache_rad_cam_state = rad_cam_state;
        }

        if( cache_sharpness != sharpness )
        {
            Shader.SetGlobalFloat("SG_Sharpness", sharpness);
            cache_sharpness = sharpness;
        }
    }
}
