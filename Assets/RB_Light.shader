﻿Shader "Unlit/RB_Light"
{
	Properties
	{
	}
	SubShader
	{
		Blend One One
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 5.0
			#pragma multi_compile SPHERE_GAUSS_4 SPHERE_GAUSS_8 SPHERE_GAUSS_12 SPHERE_GAUSS_16

			#include "UnityCG.cginc"

#ifdef SPHERE_GAUSS_4
			#define NUM_SG 4
#elif defined(SPHERE_GAUSS_8)
			#define NUM_SG 8
#elif defined(SPHERE_GAUSS_12)
			#define NUM_SG 12
#elif defined(SPHERE_GAUSS_16)
			#define NUM_SG 16
#endif

			sampler2D _NormalBuffer;
			sampler2D _TangentBuffer;
			sampler2D _DBuffer;
			sampler2D _LastCameraDepthTexture;

			float4 _LightPos;
			float _Intensity;

			float3 SG_Axii[NUM_SG];
			float SG_Sharpness;
			float SG_MonteCarloFactor;
			float SG_IntegralTerm;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 screenPos : TEXCOORD0;
				float3 viewDir : TEXCOORD1;
				float4 vertex : SV_POSITION;
			};

			struct FragOut
			{
				float energy : SV_Target0;

#ifdef SPHERE_GAUSS_4
				float4 rb0 : SV_Target1;
#elif defined(SPHERE_GAUSS_8)
				float4 rb0 : SV_Target1;
				float4 rb1 : SV_Target2;
#elif defined(SPHERE_GAUSS_12)
				float4 rb0 : SV_Target1;
				float4 rb1 : SV_Target2;
				float4 rb2 : SV_Target3;
#elif defined(SPHERE_GAUSS_16)
				float4 rb0 : SV_Target1;
				float4 rb1 : SV_Target2;
				float4 rb2 : SV_Target3;
				float4 rb3 : SV_Target4;
#endif
			};

			v2f vert (appdata v)
			{
				v2f o;

				float4 worldPos = v.vertex;

				o.vertex = UnityWorldToClipPos(v.vertex);
				o.screenPos = ComputeScreenPos(o.vertex);
				o.viewDir = -WorldSpaceViewDir(worldPos);

				return o;
			}

			float3 UnpackNormal(sampler2D tex, float2 uv)
			{
				float4 c = tex2D(tex, uv);
				return normalize(2.0*(c.xyz - 0.5));
			}

			float4 UnpackTangent(sampler2D tex, float2 uv)
			{
				float4 c = tex2D(tex, uv);
				float4 t = 2.0*(c - 0.5);
				t.xyz = normalize(t.xyz);
				return t;
			}

			FragOut frag (v2f i)
			{
				FragOut fo;

				float2 uv = i.screenPos.xy / i.screenPos.w;
				float3 normal = UnpackNormal(_NormalBuffer, uv);
				float4 tangent = UnpackTangent(_TangentBuffer, uv);
				float depth = tex2D(_DBuffer, uv).r;
				float eyedepth = _ProjectionParams.z * Linear01Depth( depth );
				float3 binormal = tangent.w * normalize(cross(normal, tangent.xyz));
				float3 viewDir = i.viewDir;

				float3 worldPos = _WorldSpaceCameraPos.xyz + (viewDir*eyedepth);
				float3 lightDir = normalize(_LightPos.xyz - worldPos);
				float lightDist = distance(worldPos, _LightPos.xyz);
				float lightStrength = _Intensity * 1.0 / (lightDist*lightDist);

				/*float3x3 TBN = float3x3(
					tangent.x, binormal.x, normal.x,
					tangent.y, binormal.y, normal.y,
					tangent.z, binormal.z, normal.z					
				);*/

				float3x3 TBN = float3x3(tangent.xyz, binormal, normal);


				/*DEBUG*/
			//	lightDir = float3(0, 0, -1);
			//	lightStrength = 1.0;


				float3 lightT = lightDir;//normalize(mul(TBN, lightDir));
				
				double totalEnergy = 0.0;
				double amplitude[NUM_SG];
				for (int i = 0; i < NUM_SG; ++i)
				{
					amplitude[i] = 0.0;

					float dp = dot(lightT, SG_Axii[i]);
					if (dp > 0.0)
					{
						double factor = (1.0 - dp) * SG_Sharpness;
						double wgt = exp(factor);
						double amp = max(SG_MonteCarloFactor * lightStrength * wgt, 0.0);
						
						double expTerm = 1.0 - exp(-2.0 * SG_Sharpness);
						double integral = 2.0 * UNITY_PI * (amp / SG_Sharpness) * expTerm;

						amplitude[i] = amp;
						totalEnergy += integral;
					}
				}

			//	double incomingEnergy = max(lightStrength * dot(float3(0,0,1), lightT), 0.00001);
				double incomingEnergy = max(lightStrength * dot(normal, lightT), 0.00001);
				totalEnergy = max(0.00001, totalEnergy);

				double normalization = (incomingEnergy/totalEnergy);

				for (int k = 0; k < NUM_SG; ++k)
				{
					amplitude[k] *= normalization;
				}

				for (int j = 0; j < NUM_SG; ++j)
				{
				//	amplitude[j] = 1.0/totalEnergy;
				}
				/*amplitude[0] = worldPos.x;
				amplitude[1] = worldPos.y;
				amplitude[2] = worldPos.z;*/

				fo.energy = incomingEnergy;

#ifdef SPHERE_GAUSS_4 
				fo.rb0 = float4(amplitude[0], amplitude[1], amplitude[2], amplitude[3]);
#elif defined(SPHERE_GAUSS_8)
				fo.rb0 = float4(amplitude[0], amplitude[1], amplitude[2], amplitude[3]);
				fo.rb1 = float4(amplitude[4], amplitude[5], amplitude[6], amplitude[7]);
#elif defined(SPHERE_GAUSS_12)
				fo.rb0 = float4(amplitude[0], amplitude[1], amplitude[2],  amplitude[3]);
				fo.rb1 = float4(amplitude[4], amplitude[5], amplitude[6],  amplitude[7]);
				fo.rb2 = float4(amplitude[8], amplitude[9], amplitude[10], amplitude[11]);
#elif defined(SPHERE_GAUSS_16)
				fo.rb0 = float4(amplitude[0],  amplitude[1],  amplitude[2],  amplitude[3]);
				fo.rb1 = float4(amplitude[4],  amplitude[5],  amplitude[6],  amplitude[7]);
				fo.rb2 = float4(amplitude[8],  amplitude[9],  amplitude[10], amplitude[11]);
				fo.rb3 = float4(amplitude[12], amplitude[13], amplitude[14], amplitude[15]);
#endif

			//	fo.rb0 = float4(lightT, 1);
			//	fo.rb0 = float4(0, 1, 0,1);
			//	fo.rb1 = float4(0, 1, 0, 1);
			//	fo.rb2 = float4(1, 0, 0, 1);
			//	fo.rb3 = float4(0, 0, 1, 1);
				return fo;
			}
			ENDCG
		}
	}
}
