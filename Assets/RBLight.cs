﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBLight : MonoBehaviour
{
    public float intensity = 1.0f;

    private float range = 1.0f;
    private Material light_material;

	void OnEnable ()
    {
        light_material = new Material(Shader.Find("Unlit/RB_Light"));
        LightManager.Get().AddLight(this);
	}

    private void OnDisable()
    {
        LightManager.Get().RemoveLight(this);
    }

    void Update ()
    {
        range = Mathf.Sqrt(intensity / 0.001f);
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color32(131, 0, 254, 255);
        Gizmos.DrawWireSphere(this.transform.position, range);
        Gizmos.DrawIcon(this.transform.position, "Light Gizmo.tiff", true);
    }

    public void Render( Camera cam )
    {
        Vector3 view_pos = cam.WorldToViewportPoint(this.transform.position);
        Vector3 right_side = cam.WorldToViewportPoint(this.transform.position - cam.transform.right * range);

        light_material.SetVector("_LightPos", this.transform.position);
        light_material.SetFloat("_Intensity", intensity);

        float d = right_side.x - view_pos.x;
        float z = cam.nearClipPlane + 0.01f;

        float left = view_pos.x - d;
        float right = right_side.x;
        float top = view_pos.y + d;
        float bot = view_pos.y - d;

        Vector3 ul = cam.ViewportToWorldPoint( new Vector3(left,  top, z) );
        Vector3 ur = cam.ViewportToWorldPoint( new Vector3(right, top, z) );
        Vector3 lr = cam.ViewportToWorldPoint( new Vector3(right, bot, z) );
        Vector3 ll = cam.ViewportToWorldPoint( new Vector3(left,  bot, z) );

        GL.PushMatrix();
        light_material.SetPass(0);
        GL.Begin( GL.QUADS );

        GL.Vertex3(ul.x, ul.y, ul.z);
        GL.Vertex3(ur.x, ur.y, ur.z);
        GL.Vertex3(lr.x, lr.y, lr.z);
        GL.Vertex3(ll.x, ll.y, ll.z);

        GL.End();
        GL.PopMatrix();
    }
}
