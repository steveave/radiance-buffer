﻿Shader "Unlit/RB_FirstPass"
{
	Properties
	{
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 tangent : TANGENT;
			};

			struct v2f
			{
				float3 normal : TEXCOORD0;
				float4 tangent : TEXCOORD1;
				float4 vertex : SV_POSITION;
			};

			struct FragOut
			{
				float4 normal : SV_Target0;
				float4 tangent : SV_Target1;
				float depth : SV_Target2;
			};
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.normal  = normalize( mul(unity_ObjectToWorld, float4(v.normal, 0.0)  ).xyz );
				o.tangent.xyz = normalize( mul(unity_ObjectToWorld, float4(v.tangent.xyz, 0.0) ).xyz );
				o.tangent.w = v.tangent.w;
				return o;
			}
			
			FragOut frag (v2f i)
			{
				FragOut fo;

				float3 n = normalize(i.normal);
				float3 t = normalize(i.tangent.xyz);

				fo.normal  = float4(0.5 * (n + 1.0), 1.0);
				fo.tangent = float4(0.5 * (t + 1.0), 0.5 * (i.tangent.w+1.0) );
				fo.depth = i.vertex.z / i.vertex.w;

				return fo;
			}
			ENDCG
		}
	}
}
