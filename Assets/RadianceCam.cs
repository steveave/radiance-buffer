﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum RADCAMDBG
{
    SCENE = -1,
    NORMALS = 0,
    TANGENTS,
    DEPTH,
    RB0,
    RB1,
    RB2,
    RB3,
    ENERGY
};

[RequireComponent(typeof(Camera))]
public class RadianceCam : MonoBehaviour
{
    public Material debug_mat;
    bool debug_enabled = false;
    public RadianceBuffer rad_buffer;

    public RadianceCam()
    {
    }

    public void SetDisplay(RADCAMDBG state)
    {
        string[] keywords =
        {
            "DEBUG_RB_NORMALS", "DEBUG_RB_TANGENTS", "DEBUG_RB_DEPTH", "DEBUG_RB_RB0", "DEBUG_RB_RB1", "DEBUG_RB_RB2", "DEBUG_RB_RB3", "DEBUG_ENERGY"
        };     

        for(int i = 0; i < keywords.Length; ++i)
        {
            if( i == (int)state )
            {
                Shader.EnableKeyword(keywords[i]);
            }
            else
            {
                Shader.DisableKeyword(keywords[i]);
            }
        }

        if ((int)state == -1)
        {
            debug_enabled = false;
        }
        else
        {
            debug_mat = new Material(Shader.Find("Hidden/RB_Debug"));
            debug_enabled = true;
        }
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        if (debug_enabled)
        {
            Graphics.Blit(null, dst, debug_mat);
        }
        else
        {
            Graphics.Blit(src, dst);
        }
    }
}
