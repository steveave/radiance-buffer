﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightManager
{
    static LightManager instance = null;

    List<RBLight> light_array;

    public LightManager()
    {
        light_array = new List<RBLight>();
    }

    public void AddLight(RBLight lgt)
    {
        light_array.Add(lgt);
    }

    public void RemoveLight(RBLight lgt)
    {
        light_array.Remove(lgt);
    }

    public static LightManager Get()
    {
        if (instance == null)
        {
            instance = new LightManager();
        }
        return instance;
    }

    public void RenderLights( Camera cam, RenderTargetSetup rts )
    {
        Graphics.SetRenderTarget(rts);

        for(int i = 0; i < light_array.Count; ++i )
        {
            light_array[i].Render(cam);
        }

        Graphics.SetRenderTarget(null);
    }
}
