﻿Shader "Unlit/RB_SecondPass"
{
	Properties
	{
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 5.0
			#pragma multi_compile SPHERE_GAUSS_4 SPHERE_GAUSS_8 SPHERE_GAUSS_12 SPHERE_GAUSS_16
			
			#include "UnityCG.cginc"

#ifdef SPHERE_GAUSS_4
			#define NUM_SG 4
#elif defined(SPHERE_GAUSS_8)
			#define NUM_SG 8
#elif defined(SPHERE_GAUSS_12)
			#define NUM_SG 12
#elif defined(SPHERE_GAUSS_16)
			#define NUM_SG 16
#endif

			sampler2D _RadBuff0;
			sampler2D _RadBuff1;
			sampler2D _RadBuff2;
			sampler2D _RadBuff3;
			sampler2D _EnergyBuff;

			float3 SG_Axii[NUM_SG];
			float SG_Sharpness;
			float SG_IntegralTerm;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
				float4 tangent : TANGENT;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 screenPos : TEXCOORD1;
				float3 viewDir : TEXCOORD2;
				float3 normal : TEXCOORD3;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.screenPos = ComputeScreenPos(o.vertex);
				o.normal = normalize(mul(unity_ObjectToWorld, float4(v.normal, 0.0)).xyz);

				TANGENT_SPACE_ROTATION;
				float3 viewDir = normalize( ObjSpaceViewDir(v.vertex) );
				o.viewDir = normalize( mul(rotation, viewDir) );

				return o;
			}

			float GGX_V1(float m2, float nDotX)
			{
				return 1.0 / (nDotX + sqrt(m2 + (1 - m2) * nDotX * nDotX));
			}
			
			float SpecularTermSGWarp(float3 normal, float3 view, float amplitudes[NUM_SG])
			{
				float specAlbedo = 0.04;
				//float3 normal = float3(0, 0, 1);
				float roughness = 0.01;

				// Create an SG that approximates the NDF. Note that a single SG lobe is a poor fit for
				// the GGX NDF, since the GGX distribution has a longer tail. A sum of 3 SG's can more
				// closely match the shape of a GGX distribution, but it would also increase the cost
				// computing specular by a factor of 3.
				float m2 = roughness * roughness;
				float specLobeSharpness = 2.0 / m2;
				float specLobeAmp = 1.0 / (UNITY_PI * m2);

				// Apply a warpring operation that will bring the SG from the half-angle domain the the
				// the lighting domain. The resulting lobe is another SG.
				float3 specLobeAxis = normalize(reflect(view, normal));
				specLobeSharpness = specLobeSharpness / (4.0 * max(dot(view, specLobeAxis), 0.1));

				// Parameters needed for evaluating the visibility term
				float nDotL = saturate(dot(normal, specLobeAxis));
				float nDotV = saturate(dot(normal, view));
				float3 h = normalize(specLobeAxis + view);

				float total = 0.0;
				float sharp_hack = SG_Sharpness;

				for (int i = 0; i < NUM_SG; ++i)
				{
					// Convolve the NDF with the SG light
					float umLength = length(specLobeSharpness * specLobeAxis + sharp_hack * SG_Axii[i]);
					float3 expo = exp(umLength - specLobeSharpness - sharp_hack) * specLobeAmp * amplitudes[i];
					float other = 1.0f - exp(-2.0f * umLength);
					float output = (2.0f * UNITY_PI * expo * other) / umLength;

					// The visibility term is evaluated at the center of our warped BRDF lobe
					output *= GGX_V1(m2, nDotL) * GGX_V1(m2, nDotV);

					// Fresnel evaluated at the center of our warped BRDF lobe
					output *= specAlbedo + (1.0f - specAlbedo) * pow((1.0f - saturate(dot(specLobeAxis, h))), 5.0f);

					// Fade out spec entirely when lower than 0.1% albedo
					output *= saturate(dot(specAlbedo, 333.0f));

					// Cosine term evaluated at the center of our warped BRDF lobe
					output *= nDotL;

					total += max(output, 0.0f);
				}

				return total;
			}

			float DiffuseLight(float amplitudes[NUM_SG])
			{
				const float3 cosLobeAxis = float3(0, 0, 1);
				const float3 cosLobeSharp = 2.33;
				const float3 cosLobeAmp = 1.17;

				float total = 0.0;

				for (int i = 0; i < NUM_SG; ++i)
				{
				//	int i = 5;
					float umLength = length(cosLobeSharp * cosLobeAxis + SG_Sharpness * SG_Axii[i]);
					float expo = exp(umLength - cosLobeSharp - SG_Sharpness) * cosLobeAmp * amplitudes[i];
					float other = 1.0 - exp(-2.0 * umLength);
					float innerProduct = (2.0 * UNITY_PI * expo * other) / umLength;
					total += max(innerProduct, 0.0);
				}

				return total;
			}

			float4 frag (v2f i) : SV_Target
			{
				float2 uv = i.screenPos.xy / i.screenPos.w;

				float amplitudes[NUM_SG];
						
				float4 rb0 = tex2D(_RadBuff0, uv);
				amplitudes[0] = rb0.x;
				amplitudes[1] = rb0.y;
				amplitudes[2] = rb0.z;
				amplitudes[3] = rb0.w;


#if defined(SPHERE_GAUSS_8) || defined(SPHERE_GAUSS_12) || defined(SPHERE_GAUSS_16)
				float4 rb1 = tex2D(_RadBuff1, uv);
				amplitudes[4] = rb1.x;
				amplitudes[5] = rb1.y;
				amplitudes[6] = rb1.z;
				amplitudes[7] = rb1.w;
#endif

#if defined(SPHERE_GAUSS_12) || defined(SPHERE_GAUSS_16)
				float4 rb2 = tex2D(_RadBuff2, uv);
				amplitudes[8] = rb2.x;
				amplitudes[9] = rb2.y;
				amplitudes[10] = rb2.z;
				amplitudes[11] = rb2.w;
#endif

#if defined(SPHERE_GAUSS_16)
				float4 rb3 = tex2D(_RadBuff3, uv);
				amplitudes[12] = rb3.x;
				amplitudes[13] = rb3.y;
				amplitudes[14] = rb3.z;
				amplitudes[15] = rb3.w;
#endif
				float incomingEnergy = tex2D(_EnergyBuff, uv).r;
				float totalEnergy = 0.0;

				for (int j = 0; j < NUM_SG; ++j)
				{
					totalEnergy += SG_IntegralTerm * amplitudes[j];
				}
				totalEnergy = max(totalEnergy, 0.0001);

				float normalization = incomingEnergy/totalEnergy;

				for (int k = 0; k < NUM_SG; ++k)
				{
				//	amplitudes[k] *= normalization;
				}

				float3 viewDir = normalize(i.viewDir);
				float3 normal = normalize(i.normal);

				float diff = DiffuseLight(amplitudes);
				float spec = SpecularTermSGWarp(normal, viewDir, amplitudes);
				float3 col = float3(0.7, 0.2, 0.1);
				diff = max(diff - spec, 0.0);

				//return float4(spec, spec, spec, 1.0);
				return float4(diff * col + spec, 1.0);
			}
			ENDCG
		}
	}
}
 