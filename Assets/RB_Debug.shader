﻿Shader "Hidden/RB_Debug"
{
	Properties
	{
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile DEBUG_RB_NORMALS DEBUG_RB_TANGENTS DEBUG_RB_DEPTH DEBUG_RB_RB0 DEBUG_RB_RB1 DEBUG_RB_RB2 DEBUG_RB_RB3 DEBUG_ENERGY

			#include "UnityCG.cginc"

#ifdef DEBUG_RB_NORMALS
			sampler2D _NormalBuffer;
#elif DEBUG_RB_TANGENTS
			sampler2D _TangentBuffer;
#elif DEBUG_RB_DEPTH
			sampler2D _DBuffer;
#elif DEBUG_RB_RB0
			sampler2D _RadBuff0;
#elif DEBUG_RB_RB1
			sampler2D _RadBuff1;
#elif DEBUG_RB_RB2
			sampler2D _RadBuff2;
#elif DEBUG_RB_RB3
			sampler2D _RadBuff3;
#elif DEBUG_ENERGY
			sampler2D _EnergyBuff;
#endif

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			float4 frag (v2f i) : SV_Target
			{
#ifdef DEBUG_RB_NORMALS
				return tex2D(_NormalBuffer, i.uv);
#elif DEBUG_RB_TANGENTS
				return tex2D(_TangentBuffer, i.uv);
#elif DEBUG_RB_DEPTH
				return tex2D(_DBuffer, i.uv);
#elif DEBUG_RB_RB0
				return tex2D(_RadBuff0, i.uv);
#elif DEBUG_RB_RB1
				return tex2D(_RadBuff1, i.uv);
#elif DEBUG_RB_RB2
				return tex2D(_RadBuff2, i.uv);
#elif DEBUG_RB_RB3
				return tex2D(_RadBuff3, i.uv);
#elif DEBUG_ENERGY
				return tex2D(_EnergyBuff, i.uv);
#else
				return float4(1.0, 0.604, 0.0863, 1.0);
#endif
			}
			ENDCG
		}
	}
}
